This directory contains subdirectories of random upstreamable utilities that may
be relevant to ChromeOS toolchain members.

These utilites should receive less scrutiny at review-time than others, and
should consequently _generally_ just be used interactively.
